const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow
const BrowserView = electron.BrowserView
let mainWindow = null


app.on('ready',()=>{
	mainWindow = new BrowserWindow({
		width: 1280,
		height: 720,
		webPreferences: {
			//下面三个设置缺一不可
			nodeIntegration: true, //设置开启nodejs环境
			contextIsolation: false,//关闭上下文隔离
			enableRemoteModule:true//允许远程引用remote模块
		    },
		 })
	mainWindow.loadFile('index.html')
	//mainWindow.webContents.openDevTools(); 
	mainWindow.on('closed',()=>{
			mainWindow = null
		})
		//1.创建BrowserView对象
		let view = new BrowserView()
		//2.装在mainWindow上
		mainWindow.setBrowserView(view)
		//3.设置大小
		view.setBounds({x:0,y:120,width:720,height:480})
		//4.设置链接
		view.webContents.loadURL('https://www.bilibili.com/')
})