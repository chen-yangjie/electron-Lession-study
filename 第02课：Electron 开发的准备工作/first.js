const {app, BrowserWindow} = require('electron');


 function createWindow () {   
    // 创建浏览器窗口
    win = new BrowserWindow({width: 800, height: 600});
    // 然后加载应用的 index.html
    win.loadFile('index.html');
	//关闭当前窗口后触发closed事件
	//on里面需要传入事件和事件函数，关闭窗口时没有定义函数，所以用箭头函数
	win.on('closed',()=>{
		console.log('closed');
		win = null;
	})

  }
   //Electron 初始化完成后触发 ready 事件 
app.on('ready', createWindow)
//  所有的窗口关闭后触发 window-all-closed 事件
app.on('window-all-closed',()=>{
	console.log('window-all-closed');
	//非 Mac OS X 平台，直接调用 app.quit() 方法退出程序
	//process.platform属性是流程模块的内置应用程序编程接口，用于获取操作系统平台信息。
	/*此属性返回代表操作系统平台的字符串。返回值可以是“ aix”，“ android”，“ darwin”，
	“ freebsd”，“ linux”，“ openbsd”，“ sunprocess”和“ win32”之一。该值在编译时设置。*/
	if (process.platform !== 'darwin') {
      app.quit();
    }
	//窗口激活后触发 activate 事件,没啥卵用
	app.on('activate',()=>{
		console.log('activate');
		if(win === null){
			createWindow();
		}
	})
})
//’ready‘本地 API 原生的事件,Electron 初始化完成后的事件 ready（类似钩子函数），app.on使得ready事件触发时，执行createWindow函数

