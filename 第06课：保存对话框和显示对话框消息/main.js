const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow
let mainWindow = null

app.on('ready',()=>{
	mainWindow = new BrowserWindow({
		width: 800,
		height: 600,
		webPreferences: {
			//下面三个设置缺一不可
			nodeIntegration: true, //设置开启nodejs环境
			contextIsolation: false,//关闭上下文隔离
			enableRemoteModule:true//允许远程引用remote模块
		    },
		 })
	mainWindow.loadFile('index.html')
	mainWindow.webContents.openDevTools(); 
	mainWindow.on('closed',()=>{
			mainWindow = null
		})
})